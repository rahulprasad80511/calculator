import React, { useState, useEffect } from "react";
import './App.css';

function App() {

  const [first, setFirst] = useState(0);
  const [sec, setSec] = useState(null);
  const [opr, setOpr] = useState(null);
  const [isPrevOpr, setIsPrevOpr] = useState(false);
  const [theme, setTheme] = useState('dark');

  const setOperand = (e) => {
    let newFirst = e;
    if(first) {
      if(isPrevOpr) {
        setSec(first);
      } else {
        newFirst = Number(first.toString()+e.toString());
      }
    }
    setFirst(newFirst);
    setIsPrevOpr(false);

  };

  useEffect(() => {
  }, [first]);

  const setOperator = (e) => {
    setIsPrevOpr(true);
      if(!opr) {
         setOpr(e);
      }
      let output = null
      switch(opr) {
        case '+':
          output = first + sec;
        break;
        case '-':
          output = sec - first;
        break;
        case '*':
          output = first * sec;
        break;
        case '/':
          output = sec / first;
        break;
        case 'square':
          output = first*first;
        break;
        case 'root':
          output = Math.sqrt(first);
        break;
      }

      if( first && sec) {
        setFirst(output);
        setSec(null)
        setOpr(e);
      } else {
         setOpr(e);
      }
  };


  const setSciOperator = (e) => {
      let output = null
      switch(e) {  
        case 'square':
          output = first*first;
        break;
        case 'root':
          output = Math.sqrt(first);;
        break;
      }
      setFirst(output);
      setOpr(null);
  };

  const clear = (e) => {
    setFirst(null);
    setSec(null);
    setOpr(null);
  }

  return (
    <div className={theme}>
      <div className="container" >
        <div className="calculator">
          <div className="result">
            <p> { first } </p>
          </div>
          <div className="row">
            <div className="col-12 theme_btn--col">
              <button className="btn theme_btn" onClick={() => setTheme('light')}>Light</button>
              <button className="btn theme_btn" onClick={() => setTheme('dark')}>Dark </button>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button className="btn" onClick={() => setOperand(1)}>1</button>
              <button className="btn" onClick={() => setOperand(2)}>2</button>
              <button className="btn" onClick={() => setOperand(3)}>3</button>
              <button className="btn action" onClick={() => setOperator('+')}>+</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button className="btn" onClick={() => setOperand(4)}>4</button>
              <button className="btn" onClick={() => setOperand(5)}>5</button>
              <button className="btn" onClick={() => setOperand(6)}>6</button>
              <button className="btn action" onClick={() => setOperator('-')}>-</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button className="btn" onClick={() => setOperand(7)}>7</button>
              <button className="btn" onClick={() => setOperand(8)}>8</button>
              <button className="btn" onClick={() => setOperand(9)}>9</button>
              <button className="btn action" onClick={() => setOperator('*')}>x</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button className="btn action" onClick={() => clear()}>clear</button>
              <button className="btn" onClick={() => setOperand(0)}>0</button>
              <button className="btn action" onClick={() => setOperator('=')}>=</button>
              <button className="btn action" onClick={() => setOperator('/')}>/</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <button className="btn action scifi" onClick={() => setSciOperator('square')}>square</button>
              <button className="btn action scifi" onClick={() => setSciOperator('root')}>root</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;